# Documentation 

## Routes :

**Important : pour les requêtes POST avec des paramètres dans le body, ajouter ce header :**  `Content-Type : application/x-www-form-urlencoded ` 
Pour tester les routes, ajouter le fichier  AngularBook.postman_collection.json dans postman (en haut à droite bouton "Import" puis entrer ce fichier)

### Debug (ces routes seront supprimées à termes) : 

***Users :*** 
* http://localhost:3000/debug/users -> récupère tous les utilisateurs  
`Method : GET `  
`RETURN {users : array of users object}`  
* http://localhost:3000/debug/users -> supprime tous les utilisateurs  
`Method : DELETE `  
`RETURN {result : object result}`  

***Posts :*** 
* http://localhost:3000/debug/posts -> récupère tous les posts         
`Method : GET `     
`RETURN { posts : array of posts object}`      
* http://localhost:3000/debug/posts -> supprime tous les posts      
`Method : DELETE `    
`RETURN {result : object result}`  
* http://localhost:3000/debug/posts/votes -> supprime tous les votes      
`Method : DELETE `    
`RETURN {result : object result}`    

***Comments :***
* http://localhost:3000/debug/comments -> récupère tous les commentaires     
`Method : GET `     
`RETURN { comments : array of comments objects }`       
* http://localhost:3000/debug/comments -> supprime tous les commentaires     
`Method : DELETE `     
`RETURN { retult : object result }`     

***Friends :***
* http://localhost:3000/debug/friends -> Supprime tous les amis acceptés, demandés, en attente de validation             
`Method : DELETE`      
`RETURN {result : object result}`    

***Messages :***     
* http://localhost:3000/debug/messages -> récupère tous les messages    
`Method : GET `     
`RETURN { result : array of messages objects } `    
* http://localhost:3000/debug/messages -> supprime tous les messages     
`Method : DELETE `     
`RETURN { result : object result } `     


***Geoloc :***  
* http://localhost:3000/debug/geo -> récupère les infos relatives à la position    
`Method : GET `
`Headers : Latitude, Longitude `  
`RETURN {"geo": {"success": bool, "rue": string, "ville": string, "dpt": string, "region": string, "pays": string, "cp": string } } `   

### Users :
* http://localhost:3000/users/login -> connexion   
`Method : POST`  
` Body : username, password `    
SI success true: `RETURN {success : true, token : string, id : string, message : string}`     
Si success false: `RETURN {success : false, message : string}`   
   
* http://localhost:3000/users/register -> inscription  
`Method : POST`  
`Body : username, password, name, firstname, (tel)`   
`RETURN {success : bool, message : string}`  
Erreur 500 si login déjà pris
 
* http://localhost:3000/users/profil/ID_USER -> profil de l'user avec l'ID correspondant  
`Method : GET`  
`Headers : token`
Si erreur : 
`RETURN {success : bool, message : string}`  
Sinon
`RETURN {json de l'user}`  

* ~~http://localhost:3000/users/search~~ -> rechercher des users     
`Method : POST`   
`Headers : token`   
` Body : search `  (ca fait comme un like en sql)
`RETURN {users : array of users objects}` 

* http://localhost:3000/users/set-profil-> enregistrer une image de profil (multipart/form-data)
`Method : POST`   
`Headers : token`   
` Body : profil`  (fichier jpeg ou png)
`RETURN {success : bool , message : string}`  (si success true `image : objet image` en plus)

* http://localhost:3000/users/remove-profil-> supprimer l'image de profil
`Method : DELETE`   
`Headers : token`   
`RETURN {success : bool , message : string}`  

* http://localhost:3000/users/set-couv-> enregistrer l'image de couverture (multipart/form-data)
`Method : POST`   
`Headers : token`   
` Body : couv`  (fichier jpeg ou png)
`RETURN {success : bool , message : string}`  (si sucess true`image : objet image` en plus)


* http://localhost:3000/users/remove-couv-> supprimer une image de couverture
`Method : DELETE`   
`Headers : token`   
`RETURN {success : bool , message : string}`  

### Posts : 
* http://localhost:3000/posts/add -> ajout d'un post       
`Method : POST`  
`Headers : token, (float) latitude, (float) longitude `  
`Body : content, (jpeg ou png) post `  
`RETURN {success : bool, message : string}`  

* http://localhost:3000/posts/post/ID_POST -> récupère un post via son ID       
`Method : GET `  
`Headers : token`   
`RETURN {success : bool, message : string ou l'objet post si success true}`  

* http://localhost:3000/posts/ID_USER/posts -> récupération des posts d'un utilisateur      
`Method : GET`  
`Headers : token`
`RETURN {success : bool, message : string ou le tableau des posts}`
* http://localhost:3000/posts/remove/ID_POST -> suppression d'un post et des commentaires associés      
`Method : DELETE`  
`Headers : token`   
`RETURN {success : bool, message : string}`  

* http://localhost:3000/posts/ID_POST/upvote -> voter pour un post      
`Method : PUT`  
`Headers : token`   
`RETURN {success : bool, message : string}`  

* http://localhost:3000/posts/ID_POST/downvote -> supprimer un vote      
`Method : PUT`  
`Headers : token`   
`RETURN {success : bool, message : string}`  

### Comments :
* http://localhost:3000/comments/ID_POST/add -> ajout d'un commentaire sur un post     
`Method : POST`     
`Header : token`     
`Body : content`     
`RETURN {success: bool, message: string, comment: comment object}`     

* http://localhost:3000/comments/remove/ID_POST/ID_COMMENT -> suppression d'un commantaire sur un post      
`Method : DELETE`     
`Header : token`     
`RETURN {success: bool, message: string}`     

### Friends
* http://localhost:3000/friends/add/ID_USER -> Demande d'ami avec l'user ayant l'ID correspondant  
`Method : POST`  
`Headers : token`  
`RETURN {success : bool, message : string}`  

* http://localhost:3000/friends/accept/ID_USER -> Acceptation d'une demande d'amis de l'user avec l'ID correspondant   
`Method : POST`   
`Headers : token`  
`RETURN {success : bool, message : string}`  

* http://localhost:3000/friends/refuse/ID_USER -> Refus d'une demande d'amis de l'user avec l'ID correspondant   
`Method : POST`  
`Headers : token`  
`RETURN {success : bool, message : string}`

* http://localhost:3000/friends/cancel/ID_USER -> Annulation d'une demande d'amis  
`Method : DELETE`  
`Headers : token` 
`RETURN {success : bool, message : string}`

* http://localhost:3000/friends/remove/ID_USER -> Suppression d'un ami  
`Method : DELETE`  
`Headers : token` 
`RETURN {success : bool, message : string}`

### Timeline
* http://localhost:3000/timeline/ -> Flux d'actualités (Postes des amis triés par date décroissante)     
`Method : GET`  
`Headers : token, (int)start, (int)limit`  
`RETURN {success : bool, message : string ou la liste des post}` 


### Messages     
* http://localhost:3000/messages/ID_USER/send -> Envoi un message à un autre utilisateur     
`Method : POST `     
`Headers : token `     
`Body : content `     
`RETURN {success : bool, message: string ou le message envoyé si success true} `     

* http://localhost:3000/messages/ID_USER/get -> récupère tous les messages privés entre l'utitilsateur en cours et un autre (envoyer ou reçu)     
`Method : GET `     
`Header : token `     
`RETURN {sucess : bool , message : string ou la liste des message si success true } `     

### Files
* http://localhost:3000/files/FOLDER_NAME/FILE_NAME ->  Affiche une image
`Method : GET`     
`Headers : token `     
`RETURN .jpeg ou .png `     
Ex avec  cette partie de json d'un profil user :   
`{
  "couv_pic": null,
  "profil_pic": {
    "_id": "58614b16e98b4a1f287d20a7",
    "name": "profil-1482771222079.png",
    "type": "profils",
    "link": "/files/profils/profil-1482771222079.png",
    "__v": 0
  },
}`

En gros il faut accéder a la propriété `link` de `profil_pic` ou `couv_pic` et le mettre en src d'une balise image en ajoutant le nom de domaine avant (http://localhost:3000 en local)  (je pense, j'ai test que sur postman)


**NB :** Le port/ip ne restera pas forcèment 3000, faire une constante pour ce genre de trucs



## Commande git :

Voir les branch :  
`git branch`

Changer de branch :  
`git checkout <branch>`

Mettre a jour sa branch :  

`git pull origin master`  
`git pull origin <branch>`

Quand erreur votre branch est en retard (impossible de push ): mettre à jour puis   
`git push`

Quand modif en cartons, pour revenir au dernier commit :  
`git reset --hard`

**Important : penser à mettre la branche master à jour régulièrement pour que la commande git diff renvoie un diff correct.**


## Commande pour faire fonctionner le serveur node 

```
npm install (quand un module est manquant)
npm start
```

## Commandes Grunt

```
grunt jshint -> dis les erreurs js sur le serveur 
```

## Autres 

Les json renvoyés par l'api possèdent ce format :
`{success : (true||false), message : "mon message", argument3 : "objet, string, int dépend ...", ... , argumentN : "blabla"}`
Tous les json contiennent un champ booléan success, si celui-ci est false le message contient un message d'erreur permettant à l'utilisateur de comprendre le problème.
