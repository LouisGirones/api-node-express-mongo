//Basic modules
var express = require('express');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');//https://www.npmjs.com/package/cors
var jwt = require('jsonwebtoken'); // jwt
var multer  =   require('multer'); // file upload


//Config
var config = require('./config');
//Database
var mongoose = require('mongoose');
require('./models/Users');
require('./models/Posts');
require('./models/Comments');
require('./models/Messages');
require('./models/Images');
mongoose.connect(config.database);

//App
app = express(); //Deguelasse c'est en global (TODO : faire un get de la config)


//Routes
var routes = require('./routes/index');
var users = require('./routes/users');
var posts = require('./routes/posts');
var friends = require('./routes/friends');
var timeline = require('./routes/timeline');
var comments = require('./routes/comments');
var messages = require('./routes/messages');
var files = require('./routes/files');

var debug = require('./routes/debug');

//Set JWT secret
app.set(config.secretName, config.secret);

// view engine setup
app.set('view engine', 'ejs');

app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', routes);
app.use('/users', users);
app.use('/posts', posts);
app.use('/friends', friends);
app.use('/timeline', timeline);
app.use('/comments', comments);
app.use('/messages', messages);
app.use('/files', files);

app.use('/debug', debug);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
