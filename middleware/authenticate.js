var jwt = require('jsonwebtoken');
var config = require('../config');

authenticate = function(req, res, next) {

  	// check header or url parameters or post parameters for token 
  	var token = req.body.token || req.query.token || req.headers.token;
  	if ( ! token) {
  		// if there is no token
    	// return an error
    	req.payload = { 
        	success: false, 
        	message: 'No token provided.' 
    	};
    	next();
    	return;
  	}

    // verifies secret and checks exp
    jwt.verify(token, app.get(config.secretName), function(err, payload) {      
    	if (err) {
        	req.payload = { 
        		success: false, 
        		message: 'Failed to authenticate token.' 
        	}; 
        	next(); 
        	return;  
    	} 
      	// if everything is good, return jwt payload
      	req.payload = {
      		success: true, 
      		payload : payload
      	};
      	next();
      	return;
    });
};

module.exports = {
	// nom quand la fonction sera appelé : nom de la fonction dans ce fichier 
    authenticate : authenticate
};