/* jshint node: true */
'use strict';

var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Image = mongoose.model('Image');
var path = require('path');
var multer = require('multer');



var upload = function(folder, fieldName) {
	//Filesize in octet
	var upload =  multer({ storage : storage(folder), fileFilter : filter(), limits : {fileSize : 1024 * 1024 , files : 1}}).single(fieldName);
	return function(req,res,next){
		if(req.payload){
			if( ! req.payload.success){
				next();
				return;
			}
		}
		upload(req,res,function(err) {
        	if(err) {
            	req.upload = {sucess : false, message : "Error uploading file."};
            	next();
            	return;
        	}
        	if(req.validationMime){
        		req.upload = {success : false, message : req.validationMime};
        		next();
        		return;
        	}
        	if( ! req.file){
        		//Succes true car facultif
                var succes = folder == 'posts' ? true : false;
        		req.upload = {success : succes, message : "No file."};
        		next();
        		return;
        	}

        	var img = new Image({
        		name   : req.file.filename,
		        type   : folder,
		        link   : '/files/' + folder + '/' + req.file.filename, //Ajouter 'http://localhost:3000/' ou whatever devant
		        size   : req.file.size
		    });

		    img.save(function (err, img) {
		        if (err) { return next(err); }
		        req.upload = { success: true, message: img };
		        next();
        		return;

		    });
    	});
	};
};


function storage(folder){
	return multer.diskStorage({
  		destination: function (req, file, callback) {
    		callback(null, './public/img/' + folder);
  		},
  		filename: function (req, file, callback) {

    		callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  		}
	});
}

function filter(){
	return function (req, file, callback) {
		var allowedMimeImage = ['image/jpeg', 'image/png'];
		//todo : check le début du fichier pour etre sur
  		if(allowedMimeImage.indexOf(file.mimetype) == -1){
  			req.validationMime = 'Seul les fichiers jpeg et png sont autorisés';
 			callback(null, false);
 			return;
 		}
 		callback(null, true);
	};
}
module.exports = {
    // nom quand la fonction sera appelé : nom de la fonction dans ce fichier
    upload : upload
};
