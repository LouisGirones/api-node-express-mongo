/* jshint node: true */
'use strict';

var express = require('express');
var router = express.Router();
var request = require('request');

var geoloc = function(req, res, next) {
    if(req.payload){
        if( ! req.payload.success){
            next();
            return;
        }
    }
    //Test cases :
    // latitude : -21.1237322 | longitude : 55.3782903 (Reunion)
    // latitude : 48.73925530 | longitude : 2.29919760 (Massy)
    var lat = req.body.latitude || req.query.latitude || req.headers.latitude;
    var long = req.body.longitude || req.query.longitude || req.headers.longitude;
    
    if( ! lat || ! long ){
        req.geoloc = {
            success : false ,
            message : 'Une des coordonnées n\'a pas été renseignée'
        };
        next();
        return;
    }
    var coord = lat + ',' + long;
    //http://stackoverflow.com/questions/3518504/regular-expression-for-matching-latitude-longitude-coordinates
    var pattern = new RegExp(/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/);
    if( ! pattern.test(coord)){
        req.geoloc = {
            success : false ,
            message : 'Une des coordonnées est incorrecte'
        };
        next();
        return;
    }

    var options = {  
        url: 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + coord + '&sensor=true',
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8'
        }
    };

    request(options, function(err, res, body) {  
        var json = JSON.parse(body);

        if(json.status == 'ZERO_RESULTS'){
            req.geoloc = {
                success : false ,
                message : 'Aucun résultat'
            };
            next();
            return;
        }

        var arrAddress = json.results[0].address_components;
        var route, ville, dpt, region, pays, cp = null;

        for(var i = 0; i<arrAddress.length; i++){
            switch(arrAddress[i].types[0]){
                case 'route' : 
                    route = arrAddress[i].long_name;
                break;
                case "locality" :
                    ville = arrAddress[i].long_name;
                break;
                case "administrative_area_level_2" :
                    dpt = arrAddress[i].long_name; 
                break;
                case "administrative_area_level_1" : 
                    region = arrAddress[i].long_name;
                break;
                case "country" :
                    pays = arrAddress[i].long_name;
                break;
                case "postal_code" :
                    cp = arrAddress[i].long_name;
                break;
            }
        }

        req.geoloc = {
            success : true, 
            route   : route,
            ville   : ville,
            dpt     : dpt,
            region  : region,   
            pays    : pays,
            cp      : cp
        };
        next();
        return;
    });
};

module.exports = {
    // nom quand la fonction sera appelé : nom de la fonction dans ce fichier
    geoloc : geoloc
};

