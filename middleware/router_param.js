var mongoose = require('mongoose');
var User = mongoose.model('User');
var Post = mongoose.model('Post');
var Comment = mongoose.model('Comment');

getID = function(req, res, next, id) {
  var query = User.findById(id)
  .populate({path: 'friends_pending friends_asked friends_accepted', populate: { path:  'profil_pic couv_pic', model: 'Image'}})
  .populate('profil_pic couv_pic');
  query.exec(function (err, user){
    if (err) { return next(err); }
    req.user = user;
    return next();
  });
};

getPOST = function (req, res, next, id) {
    var query = Post.findById(id).populate({path: 'comments', populate: { path:  'postedBy', model: 'User' , select : 'username'}}).populate('picture');
    query.exec(function (err, post) {
        if (err) { return next(err); }
        req.post = post;
        return next();
    });
};


getCOMMENT = function (req, res, next, id) {
    var query = Comment.findById(id);
    query.exec(function (err, comment) {
        if (err) { return next(err); }
        req.comment = comment;
        return next();
    });
};

module.exports = {
	// nom quand la fonction sera appelé : nom de la fonction dans ce fichier
    getID : getID,
    getPOST : getPOST,
    getCOMMENT : getCOMMENT
};