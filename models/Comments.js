/* jshint node: true */
'use strict';

var mongoose = require('mongoose');

var CommentSchema = new mongoose.Schema({
    content: { type: String, required: true },
    postedBy: 
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    date_comment: { type: Date, default: Date.now }
});

module.exports = mongoose.model("Comment", CommentSchema);