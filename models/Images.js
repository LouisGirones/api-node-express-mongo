/* jshint node: true */
'use strict';

var mongoose = require('mongoose');

var ImageSchema = new mongoose.Schema({
    name : {type : String, required : true},
    type : {type : String, required : true},
    link : {type : String, required : true},
    size : {type : Number,  select : false, required : true}
});

module.exports = mongoose.model('Image', ImageSchema);