/* jshint node: true */
'use strict';

var mongoose = require('mongoose');

var MessageSchema = new mongoose.Schema({
    content: { type: String, required: true },
    date_msg: { type: Date, default: Date.now },
    //TODO : Créer un model Conversatiopn qui contient un tableau d'utilisateur et mettre juste conv et senby
    send_by: 
    { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true 
    },
    send_to: 
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});

module.exports = mongoose.model("Message", MessageSchema);