/* jshint node: true */
'use strict';

var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
    //Contenu
    content : { type: String, required: true },
    //Geoloc
    route   : {type : String, default : null},
    ville   : {type : String, default : null},
    dpt     : {type : String, default : null},
    region  : {type : String, default : null},
    pays    : {type : String, default : null},
    cp      : {type : String, default : null},
    //Date
    datePost: { type: Date, default: Date.now },
    //Auteur
    postedBy: 
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    //Image 
    picture:
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Image',
        default: null  
    },
    //Votes
    votes   : 
    [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            unique : true
        }   
    ],
    //Commentaires
    comments: 
    [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Comment'
        }
    ]
});

PostSchema.pre('save', function (next) {
    var self = this;
    //TODO : ajouter une heure ou faire un truc plus propre
    self.datePost = Date.now;
    next();
});

module.exports = mongoose.model("Post", PostSchema);