/* jshint node: true */
/*jshint esversion: 6 */
'use strict';

var mongoose = require('mongoose'),
    bcrypt = require('bcrypt-nodejs');

const SALT_WORK_FACTOR = 10;

var UserSchema = new mongoose.Schema({
    // Personal info
    username    : {type: String, unique: true, required : true},
    password    : {type : String,  select : false, required : true},
    name        : {type : String },
    firstname   : {type : String },
    tel         : {type : String},
    date_inscr  : { type: Date, default: Date.now },
    //Image de profil 
    profil_pic  :
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Image',
        default: null
    },
    //Image de couverture
    couv_pic    :
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Image',
        default: null  
    },
    //**************
    // Relationships
    // *************
    
    // Attente
    friends_pending : 
    [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
      }
    ],
    //Accepté
    friends_accepted : 
    [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
      }
    ],
    //Demandé
    friends_asked : 
    [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
      }
    ]
});

UserSchema.pre('save', function (next) {
    var self = this;
    // only hash the password if it has been modified (or is new)
    if ( ! self.isModified('password')) return next();
    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) return next(err);
        // hash the password along with our new salt
        bcrypt.hash(self.password, salt, null, function (err, hash) {
            if (err) return next(err);
            // override the cleartext password with the hashed one
            self.password = hash;
            next();
        });
    });
});

UserSchema.pre('save', function (next) {
    var self = this;
var Image = mongoose.model('Image');

    var imgProfil = new Image({
            name   : 'default',
            type   : 'default',
            link   : '/files/default/user_img.png', 
            size   : 500
        });
    var imgCouv = new Image({
            name   : 'default',
            type   : 'default',
            link   : '/files/default/couv_img.png', 
        size   : 500
    });

    imgProfil.save(function (err, img) {
            if (err) { return next(err); }
            self.profil_pic = imgProfil;
    });
    imgCouv.save(function (err, img) {
            if (err) { return next(err); }
            self.couv_pic = imgCouv;
            next();
    });
});


/********************************/
//General 
/********************************/

UserSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) return cb(err);
    cb(err, isMatch);
  });
};

UserSchema.methods.getUserByName = function(name, param, cb){
  this.model('User').findOne({ username : name }).select(param).exec(function (err, user) {
    if (err) { return cb(err); }
    cb(err, user);
  });
};


/********************************/
//Friends 
/********************************/

UserSchema.methods.Friends = function(userId, friendId, userParams, friendParams, cb){
  //Update user
  this.model('User').findOneAndUpdate({_id : userId}, userParams, function(err, user){
    if(err){return cb(err);}
  });

  //Update friend
  this.model('User').findOneAndUpdate({_id : friendId}, friendParams, function(err, user){
    if(err){return cb(err);}
  });
  cb(null, true);
};

module.exports = mongoose.model('User', UserSchema);