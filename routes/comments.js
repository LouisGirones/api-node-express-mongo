var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var User = mongoose.model('User');
var Post = mongoose.model("Post");
var Comment = mongoose.model("Comment");
var auth = require('../middleware/authenticate');
var param = require('../middleware/router_param');

/* POST comment */
router.post('/:post/add', auth.authenticate, function (req, res, next) {
    if (!req.payload.success) {
        res.status(403).json(req.payload);
        return;
    }
    var post = new Post(req.post);


    if (!post.equals(req.post)) {
        res.json({ success: false, message: 'Le poste n\'existe pas' });
        return;
    }
    var user = new User(req.payload.payload);

    User.findById(user._id ,function(err, user){
        //Possible de commenter uniquement ses postes et ceux de ses amis
        if(  user.friends_accepted.indexOf(post.postedBy) == -1 && ! user._id.equals(post.postedBy)){
            res.json({ success: false, message: 'Vous pouvez commenter uniquement vos postes et ceux de vos amis' });
            return;
        } 
        var c = new Comment({
            content: req.body.content,
            postedBy: req.payload.payload._id
        });
        c.save(function (err, comment) {
            if (err) { return next(err); }
            post.update({$addToSet : {comments: c._id}}).exec(function (err, post) {
                if (err) { return next(err); }
                res.json({ success: true, message: 'Commentaire ajouté', comment: c });
            });
        });
    });
});

/* DELETE comment */
router.delete('/remove/:post/:comment', auth.authenticate, function (req, res, next) {
    if (!req.payload.success) {
        res.status(403).json(req.payload);
        return;
    }

    var comment = new Comment(req.comment);
    var post = new Post(req.post);
    var user = new User(req.payload.payload);

    if (!post.equals(req.post)) {
        res.json({ success: false, message: 'Le poste n\'existe pas' });
        return;
    }
    if (!comment.equals(req.comment)) {
        res.json({ success: false, message: 'Le commentaire n\'existe pas' });
        return;
    }
    if (!comment.postedBy.equals(user._id)) {
        res.json({ success: false, message: 'Impossible de supprimer ce commentaire' });
        return;
    }
    post.update({ $pull: { comments: comment._id } }).exec(function (err, post) {
        if (err) { return next(err); }
        comment.remove(function (err, result) {
            if (err) { return next(err); }
            res.json({ success: true, message: 'Le commentaire à bien été supprimé' });
        });
    });
});

router.param('post', param.getPOST);
router.param('user', param.getID);
router.param('comment', param.getCOMMENT);

module.exports = router;