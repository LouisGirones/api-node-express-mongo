var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Post = mongoose.model('Post');
var Comment = mongoose.model('Comment');
var Message = mongoose.model('Message');
var Image = mongoose.model('Image');
var param = require('../middleware/router_param');
var geolocation = require('../middleware/geolocation');
var file = require('../middleware/file');
var jwt = require('jsonwebtoken');
var config = require('../config'); 


/****************
Files
****************/

/* GET image */
router.get('/images', function(req, res, next) {
  //Populate allow to retrieve an object with User property instead of just the User._id
  //User.find().find().populate('friends_accepted friends_pending friends_asked').exec(function(err, users){
  Image.find().exec(function(err, images){
    if(err){ return next(err); }
    res.json({image : images});
  });
});

/* DELETE image (de la bdd pas les fichiers) */
router.delete('/images', function (req, res, next) {
  Image.remove().exec(function (err, result) {
    if (err) { return cb(err); }
    res.json({result : result});
  });
});


/*Upload */

router.post('/upload', file.upload('debug','debug') , function(req, res, next){
    res.json({upload : req.upload});

});

/****************
Friends
****************/

/* DELETE relationships  */
router.delete('/friends', function(req, res, next) {
  User.update({}, {$unset: {friends_pending : 1, friends_asked : 1, friends_accepted : 1  }}, {multi : true}, function(err, resultat){
    if(err){ return next(err); }
    res.json({result : resultat});
  });
});


/****************
Users
****************/

/* GET users */
router.get('/users', function(req, res, next) {
  //Populate allow to retrieve an object with User property instead of just the User._id
  //User.find().find().populate('friends_accepted friends_pending friends_asked').exec(function(err, users){
  User.find().exec(function(err, users){
    if(err){ return next(err); }
    res.json({users : users});
  });
});

/* DELETE users */
router.delete('/users', function (req, res, next) {
  User.remove().exec(function (err, result) {
    if (err) { return cb(err); }
    res.json({result : result});
  });
});


/****************
Posts
****************/

/* GET all posts */
router.get('/posts', function (req, res, next) {
    Post.find().exec(function (err, posts) {
        if (err) { return next(err); }
        res.json({nb :posts.length, posts : posts });
    });
});

/* GET post by ID */
router.get('/post/:post', function (req, res, next) {
    res.json(req.post);
});

/* DELETE all posts */
router.delete('/posts', function (req, res, next) {
    Post.remove({}).exec(function (err, result) {
        if (err) { return next(err); }
        res.json({ result : result });
    });
});

/* DELETE all votes   */
router.delete('/posts/votes', function(req, res, next) {
  Post.update({}, {$unset: {votes : 1}}, {multi : true}, function(err, resultat){
    if(err){ return next(err); }
    res.json({result : resultat});
  });
});


/****************
Comments
****************/

/* GET all comments */
router.get('/comments', function (req, res, next) {
    Comment.find().exec(function (err, comments) {
        if (err) { return next(err); }
        res.json(comments);
    });
});

/* DELETE all comment */
router.delete('/comments', function (req ,res, next) {
  Comment.remove({}).exec(function (err, result) {
    if (err) { return next(err); }
    res.json(result);
  });
});

/****************
Geoloc
****************/
router.get('/geo', geolocation.geoloc, function(req, res, next){

  res.json({ geo : req.geoloc});
});

/****************
Messages
****************/

/* GET all messages */
router.get('/messages', function (req, res, next) {
  Message.find().exec(function (err, result) {
    if (err) { return next(err); }
    res.json(result);
  });
});

/* DELETE all messages */
router.delete('/messages', function (req, res, next) {
  Message.remove({}).exec(function (err, result) {
    if (err) { return next (err); }
    res.json(result);
  });
});

router.param('post', param.getPOST);
router.param('user', param.getID);
router.param('comment', param.getCOMMENT);

module.exports = router;