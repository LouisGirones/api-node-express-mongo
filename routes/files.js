var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var auth = require('../middleware/authenticate');

 
router.get('/:folder/:name', auth.authenticate , function(req, res, next){

    if( ! req.params.folder || ! req.params.name){
        res.status(404).json({success : false, message : 'impossible de récupérer l\'image'});
        return;
    }
    var folder = req.params.folder;
    var options = {
        root:  './public/img/' +folder,
        dotfiles: 'deny',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true
        }
    };

    var fileName = req.params.name;

    res.sendFile(fileName, options, function (err) {
        if (err) {
            if(err.status == 404){
                res.json({success : false, msg : 'fichier introuvable'});
                return;
            }
            return next(err);
        }
    });
});

module.exports = router;