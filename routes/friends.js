var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');
var jwt = require('jsonwebtoken');
var auth = require('../middleware/authenticate');
var param = require('../middleware/router_param');

/********************************/
//Create friend request 
/********************************/
router.post('/add/:user', auth.authenticate, function(req, res, next){
    if( ! req.payload.success){
        res.status(403).json(req.payload);
        return;
    }
    //L'utilisateur que l'on demande en amis
    var friend = new User(req.user);
    //Ajout dans friends_asked pour l'utilisateur en cours
    var userParams = {$addToSet : {friends_asked : friend._id}};
    //Ajout dans friends_pending pour l'utilisateur demandé
    var friendParams = {$addToSet : {friends_pending : req.payload.payload._id}};

    friendsRoutesBuilder("add", req.payload.payload._id, friend, userParams, friendParams, function(success, message){
      res.json({success : success, message : message});
    }); 
});

/********************************/
//Accept friend request
/********************************/
router.post('/accept/:user', auth.authenticate, function(req, res, next){
    if( ! req.payload.success){
        res.status(403).json(req.payload);
        return;
    }
    //L'utilisateur que l'on accepte en amis
    var friend = new User(req.user);
    //On supprime la demande et ajoute en ami
    var userParams = {$pull : {friends_pending : friend._id}, $addToSet : {friends_accepted : friend._id}};
    //On supprime la demande en attente et ajoute en ami
    var friendParams = {$pull : {friends_asked : req.payload.payload._id}, $addToSet : {friends_accepted : req.payload.payload._id}};

    friendsRoutesBuilder("accept", req.payload.payload._id, friend, userParams, friendParams, function(success, message){
      res.json({success : success, message : message});
    }); 
});

/********************************/
//Don't accept friend request
/********************************/
router.post('/refuse/:user', auth.authenticate, function(req, res, next){
    if( ! req.payload.success){
        res.status(403).json(req.payload);
        return;
    }
    //L'utilisateur que l'on accepte en amis
    var friend = new User(req.user);
    //On supprime la demande 
    var userParams = {$pull : {friends_pending : friend._id}};
    //On supprime la demande en attente et ajoute en ami
    var friendParams = {$pull : {friends_asked : req.payload.payload._id}};

    friendsRoutesBuilder("refuse", req.payload.payload._id, friend, userParams, friendParams, function(success, message){
      res.json({success : success, message : message});
    }); 
});


/********************************/
//Cancel friend request
/********************************/
router.delete('/cancel/:user', auth.authenticate, function(req, res, next){
    if( ! req.payload.success){
        res.status(403).json(req.payload);
        return;
    }
    //L'utilisateur que l'on accepte en amis
    var friend = new User(req.user);
    //On supprime la demande 
    var userParams = {$pull : {friends_asked : friend._id}};
    //On supprime la demande en attente et ajoute en ami
    var friendParams = {$pull : {friends_pending : req.payload.payload._id}};

    friendsRoutesBuilder("cancel", req.payload.payload._id, friend, userParams, friendParams, function(success, message){
      res.json({success : success, message : message});
    }); 
});


/********************************/
//Remove friend
/********************************/
router.delete('/remove/:user', auth.authenticate, function(req, res, next){
    if( ! req.payload.success){
        res.status(403).json(req.payload);
        return;
    }
    //L'utilisateur que l'on supprime
    var friend = new User(req.user);
    //On supprime la demande 
    var userParams = {$pull : {friends_accepted : friend._id}};
    //On supprime la demande en attente et ajoute en ami
    var friendParams = {$pull : {friends_accepted : req.payload.payload._id}};

    friendsRoutesBuilder("remove", req.payload.payload._id, friend, userParams, friendParams, function(success, message){
      res.json({success : success, message : message});
    }); 
});

//Allow to set an user id in the url 
router.param('user', param.getID);

//TODO : creer un ou deux modules pous ces functions

/**
 * Template pour les routes friends
 * @param  {[string]} action       La route
 * @param  {[int]}    userId       L'identifiant de l'utilisateur en cours (payload)
 * @param  {[object]} friend       L'utilisateur avec lequel on interagit
 * @param  {[object]} userParams   Les modifs sur l'utilisateur en cours
 * @param  {[object]} friendParams Les modifs sur "l'ami"
 * @return {[JSON]}                {success : bool, message : string}
 */
function friendsRoutesBuilder(action, userId, friend, userParams, friendParams, cb){
  //L'utilisateur ayant fait la demande
  User.findById(userId).exec(function(err, user){
      //Récupération des "droits"
      //TODO : ajouter un callback
      var valid = testForFriends(action, user, friend._id, friend.username);

      //Vérification des "droits"
      if( ! valid.success){
        cb(valid.success, valid.message);
        return;
      }

      user.Friends(userId, friend._id, userParams, friendParams, function(err, result){
         if (err) throw err;
         cb(valid.success, valid.message);
      });
   });
}

/**
 * Vérifie l'action 
 * @param  {[string]} action      La route a testé
 * @param  {[object]} user        L'utilisateur en cours
 * @param  {[int]}    friendId    L'identifiant de l'utilisateur avec lequel on interagit
 * @param  {[string]} friendName  L'username de l'utilisateur avec lequel on interagit
 * @return {[JSON]}               {success : bool, message : string}
 */
function testForFriends(action, user, friendId, friendName){
    switch(action){

        case "add" : 
            if(String(user._id) == String(friendId)){
              return { success : false, message : 'Vous ne pouvez pas être amis avec vous même'};
            }

            if( user.friends_asked.indexOf(friendId) != -1){
              return { success : false, message : 'Vous avez dèjà envoyé une demande à : ' + friendName};
            }
            
            if( user.friends_pending.indexOf(friendId) != -1){
              return { success : false, message : friendName + ' vous a déjà envoyé une demande'};
            }

            if( user.friends_accepted.indexOf(friendId) != -1){
              return { success : false, message : 'Vous êtes déjà amis avec : ' + friendName};
            }

            return {success : true, message : 'L\'utilisateur ' + friendName + ' a bien reçu votre demande d\'amis'};
        break;

        //TODO : check si les 2 users sont amis
        case "accept" : 
        case "refuse" :
            //Pas de demande 
            if(user.friends_pending.indexOf(friendId) == -1 ){
                return {success : false, message : friendName + ' ne vous a pas demandé en amis'};
            }
            var msg = action == "accept" ? 'Vous et ' + friendName + ' êtes maintenant amis' : 'Vous avez refusé la demande d\'amis de ' + friendName;
            return {success : true, message : msg};
        break;

        case "cancel" :
            //Pas de demande envoyée
            if(user.friends_asked.indexOf(friendId) == -1 ){
                return {success : false, message : 'Vous n\'avez pas demandé ' + friendName + ' en amis' };
            }
            return {success : true, message : 'Votre demande d\'amis avec ' + friendName + ' à bien été supprimée'};
        break;

        case "remove" :
            //Pas amis
            if(user.friends_accepted.indexOf(friendId) == -1 ){
                return {success : false, message : 'Pas amis' };
            }
            return {success : true, message : 'Vous n\'êtes plus amis avec ' + friendName };
        break;

        default:
            //TODO ; ajouter un log
            return {success : true, message : 'What the fuckin fuck'};
    }
}

module.exports = router;