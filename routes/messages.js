var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var User = mongoose.model('User');
var Message = mongoose.model('Message');
var auth = require('../middleware/authenticate');
var param = require('../middleware/router_param');

/*POST msg */
router.post('/:user/send', auth.authenticate, function (req, res, next) {
    if (!req.payload.success) {
        res.status(403).json(req.payload);
        return;
    }
    var sender = new User(req.payload.payload);
    var recever = new User(req.user);
    if (!recever.equals(req.user)) {
        res.json({ success: false, message: 'Cet utilisateur n\'existe pas' });
        return;
    }

    var msg = new Message({
        content: req.body.content,
        send_by: sender._id,
        send_to: recever._id
    });

    msg.save(function (err, msg) {
        if (err) { return next(err); }
        res.json({ success: true, message: msg });
    });
});

/*GET messages between 2 users */
router.get('/:user/get', auth.authenticate, function (req, res, next) {
    if (!req.payload.success) {
        res.status(403).json(req.payload);
    }
    var user = new User(req.payload.payload);
    var friend = new User(req.user);
    if (!friend.equals(req.user)) {
        res.json({ success: false, message: 'Cet utilisateur n\'existe pas' });
        return;
    }

    Message.find({ $or :[ {$and :[{send_to : friend._id, send_by : user._id}]}, {$and :[{send_to : user._id, send_by : friend._id}]}  ] })
    .sort({date_msg : -1 })
    .populate('send_by send_to')
    .exec(function (err, messages) {
        if (err) { return next(err); }
        res.json({success : true, message : messages});
    });
});

router.param('user', param.getID);

module.exports = router;