var express     = require('express');
var router      = express.Router();
var mongoose    = require('mongoose');
var jwt         = require('jsonwebtoken');
var User        = mongoose.model('User');
var Post        = mongoose.model("Post");
var Comment     = mongoose.model('Comment');
var Image       = mongoose.model('Image');
var auth        = require('../middleware/authenticate');
var param       = require('../middleware/router_param');
var geolocation = require('../middleware/geolocation');
var file        = require('../middleware/file');
var fs          = require('fs');

/* GET posts by post ID */
router.get('/post/:post', auth.authenticate, function (req, res, next) {
    if (!req.payload.success) {
        res.status(403).json(req.payload);
        return;
    }
    res.json({success : true , message :req.post});
});

/* POST post */
router.post('/add',auth.authenticate, geolocation.geoloc, file.upload('posts','post') , function(req, res, next){
    if (!req.payload.success) {
        res.status(403).json(req.payload);
        return;
    }
  
    var p = new Post({
        content : req.body.content,
        postedBy: req.payload.payload._id,
        picture : req.upload.message._id,
        route   : req.geoloc.route,
        ville   : req.geoloc.ville,
        dpt     : req.geoloc.dpt,
        region  : req.geoloc.region,
        pays    : req.geoloc.pays,
        cp      : req.geoloc.cp
    });
    p.save(function (err, post) {
        if (err) { return next(err); }
        res.json({ success: true, message: p });
    });
});



/* REMOVE post by ID */
router.delete('/remove/:post', auth.authenticate, function (req, res, next) {
    if (!req.payload.success) {
        res.status(403).json(req.payload);
        return;
    }
    var user = new User(req.payload.payload);
    var post = new Post(req.post);

    if( ! post.equals(req.post)){
        res.json({ success : false, message : 'Le post n\'existe pas'});
        return;
    }

    if( ! post.postedBy.equals(user._id)){
        res.json({ success : false, message : 'Impossible de supprimer le poste d\'un autre utilisateur'});
        return;
    }

    Comment.remove({ _id : {$in : post.comments}}).exec(function(err, result){
        if (err) { return next(err); }
        Image.remove({ _id : post.picture}).exec(function(err, result){
            Post.remove({ _id: post._id }).populate('picture').exec(function (err, result) {
                if (err) { return next(err); }
                if(post.picture && post.picture.name){
                    fs.unlinkSync('./public/img/posts/' + post.picture.name);
                }
                res.json({ success: true, message: 'Le poste a bien été supprimé' });
            });
        });
    });
});

/* Get one user's posts */
router.get('/:user/posts', auth.authenticate, function (req, res, next) {
    if (!req.payload.success) {
        res.status(403).json(req.payload);
    }
    var user = new User(req.user);
    Post.find({ postedBy: user._id })
    .populate('postedBy', 'username')
    .populate('picture', 'link')
    .populate({path: 'comments', populate: { path:  'postedBy', model: 'User' , select : 'username'}}) 
    .sort({datePost : -1 })
    .exec(function (err, posts) {
        if (err) { return next(err); }
        res.json({success : true, message : posts });
    });
});

/*Upvote a post */
router.put('/:post/upvote', auth.authenticate, function(req, res, next){
    if (!req.payload.success) {
        res.status(403).json(req.payload);
    }
    var user = new User(req.payload.payload);

    var post = new Post(req.post);
    if( ! post.equals(req.post)){
        res.json({ success : false, message : 'Le post n\'existe pas'});
        return;
    }

    post.update({$addToSet : {votes : user._id}}).exec(function (err, post) {
        if (err) { return next(err); }
        res.json({ success: true, message: 'Votre vote a bien été enregistré'});
    });
});


/*Remove post upvote */
router.put('/:post/downvote', auth.authenticate, function(req, res, next){
    if (!req.payload.success) {
        res.status(403).json(req.payload);
    }
    var user = new User(req.payload.payload);

    var post = new Post(req.post);
    if( ! post.equals(req.post)){
        res.json({ success : false, message : 'Le post n\'existe pas'});
        return;
    }

    post.update({$pull : {votes : user._id}}).exec(function (err, post) {
        if (err) { return next(err); }
        res.json({ success: true, message: 'Votre vote a bien été supprimé'});
    });
});

router.param('post', param.getPOST);
router.param('user', param.getID);

module.exports = router;