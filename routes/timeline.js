var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var User = mongoose.model('User');
var Post = mongoose.model("Post");
var auth = require('../middleware/authenticate');
var param = require('../middleware/router_param');

//Get timeline of one user
router.get('/', auth.authenticate, function(req,res,next){
    if (!req.payload.success) {
        res.status(403).json(req.payload);
        return;
    }

    var user = new User(req.payload.payload),
        posts = {},
        start = parseInt(req.body.start || req.query.start || req.headers.start),
        limit = parseInt(req.body.limit || req.query.limit || req.headers.limit);

    if(user.friends_accepted === 0){
        res.json({sucess : true, message : 'Ajoutez des amis pour une expérience plus amusante'});
        return;
    }
    User.findById(user._id, function(err, user){
        Post.find()
        .where('postedBy')
        //.populate({path: 'comments', populate: { path:  'postedBy', model: 'User' , select : 'username'}})
        .populate('picture postedBy')
        .in(user.friends_accepted)
        .sort({datePost : -1 })
        .skip(start).limit(limit)
        .exec(function(err, posts){
            if(err){ return next(err);}
            res.json({sucess : true, message : posts });
        });
    });
});

router.param('post', param.getPOST);
router.param('user', param.getID);

module.exports = router;