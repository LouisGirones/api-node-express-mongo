var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Image = mongoose.model('Image');
var jwt = require('jsonwebtoken');
var config = require('../config');
var auth = require('../middleware/authenticate');
var param = require('../middleware/router_param');
var file = require('../middleware/file');
var fs = require('fs');

/* POST login */
router.post('/login', function (req, res, next) {
  if(! req.body.username || ! req.body.password){
    res.status(403).json({ success: false, message: 'Vous devez entrer une adresse email et un mot de passe' });
    return;
  }
  var user = new User(req.body);
  var pass = req.body.password;

  var params = { "username"  : 1, "password"  : 1 };

  user.getUserByName(user.username, params, function(err, user){
    if(err){ throw err; }

    //No user found
    if(user === null){
      res.status(403).json({ success: false, message: 'Utilisateur non trouvé' });
      return;
    }
    //Test if the entered password match the hash
    user.comparePassword(pass, function (err, isMatch) {
      if (err) throw err;
      //No match for the password
      if ( ! isMatch) {
        res.status(403).json({ success: false, message: 'Mauvais identifiants' });
        return;
      }
      //Creating the token
      user.password = null;
      var token = jwt.sign(user.toObject(), app.get(config.secretName), {
        expiresIn: '24h' // expires in 24 hours
      });
      res.json({ success: true, token: token, id : user._id, message : 'Connection réussie' });
    });
  });
});

/* POST register */
router.post('/register', function (req, res, next) {
  if(! req.body.username || ! req.body.password){
    res.status(403).json({ success: false, message: 'Vous devez entrer une adresse email et un mot de passe' });
    return;
  }
  var user = new User(req.body);

  user.save(function(err, result){
    if (err) { 
      if (err.name === 'MongoError' && err.code === 11000) {
        // Duplicate username
        return res.status(500).send({ succes: false, message: 'Cette adresse email est déjà enregistrée.' });
      }
      // Some other error
      return res.status(500).send(err);
    }
    res.json({ success: true, message: "Le compte a bien été enregistré" });
  });
});

/*POST search users */
router.post('/search', auth.authenticate, function(req, res, next){
  if (!req.payload.success) {
    res.status(403).json(req.payload);
    return;
  }
  var search = req.body.search;
  if( ! search){
    res.json({users : []});
    return;
  }
  User.find({username : {$regex : search }}).populate('couv_pic profil_pic').exec(function(err, users){
    if(err) { return next(err);}
    res.json({users : users});
  });
});

/* GET user profil */
router.get('/profil/:user', auth.authenticate, function (req, res, next) {
  if (!req.payload.success) {
    res.status(403).json(req.payload);
    return;
  }
  // TODO : select data selon lien d'amitié
  //On renvoie l'user
  res.json(req.user);
});

/*POST set user profil picture*/
router.post('/set-profil', auth.authenticate, file.upload('profils','profil'), function(req, res, next){
    if (!req.payload.success) {
        res.status(403).json(req.payload);
        return;
    }
    if(!req.upload.success){
        res.status(400).json(req.upload);
        return;
    }

    var image = new Image(req.upload.message);
    User.findById(req.payload.payload._id).populate('profil_pic').exec(function (err, user) {
        if(user.profil_pic ){
            Image.find().remove({ _id: user.profil_pic }).exec();
            if(user.profil_pic.name && user.profil_pic.name != "default"){
                fs.unlinkSync('./public/img/profils/' + user.profil_pic.name);                
            }
        }
        User.findByIdAndUpdate(user._id, { $set: { profil_pic: image._id }}, { new: true }, function (err, user) {
            if (err) return next(err);
            res.json({success : true, message : 'L\image a bien été téléchargée', image : image });
        });
    });    
}); 

/*DELETE user profil picture*/
router.delete('/remove-profil', auth.authenticate, function(req, res, next){
    if (!req.payload.success) {
        res.status(403).json(req.payload);
        return;
    }

    User.findById(req.payload.payload._id).populate('profil_pic').exec(function (err, user) {
        if(user.profil_pic === null){
            res.json({success : true, message : 'pas d\'image a supprimé'});
            return;
        }
        Image.find().remove({ _id: user.profil_pic }).exec();
        if(user.profil_pic.name && user.profil_pic.name != "default"){
            fs.unlinkSync('./public/img/profils/' + user.profil_pic.name);
        }
        User.findByIdAndUpdate(user._id, { $set: { profil_pic: null }}, { new: true }, function (err, user) {
            if (err) return next(err);
            res.json({success : true, message : 'L\'image a bien été supprimée'});
        });
    });    
}); 

/*POST set user couv picture*/
router.post('/set-couv', auth.authenticate, file.upload('couvs','couv'), function(req, res, next){
    if (!req.payload.success) {
        res.status(403).json(req.payload);
        return;
    }
    if(!req.upload.success){
        res.status(400).json(req.upload);
        return;
    }

    var image = new Image(req.upload.message);
    User.findById(req.payload.payload._id).populate('couv_pic').exec(function (err, user) {
        if(user.couv_pic){
            Image.find().remove({ _id: user.couv_pic }).exec();
            if(user.couv_pic.name && user.couv_pic.name != "default"){
                fs.unlinkSync('./public/img/couvs/' + user.couv_pic.name);
            }
        }
        User.findByIdAndUpdate(user._id, { $set: { couv_pic: image._id }}, { new: true }, function (err, user) {
            if (err) return next(err);
            res.json({success : true, message : 'L\image a bien été téléchargée', image : image });
        });
    });    
});

/*DELETE user couv picture*/
router.delete('/remove-couv', auth.authenticate, function(req, res, next){
    if (!req.payload.success) {
        res.status(403).json(req.payload);
        return;
    }

    User.findById(req.payload.payload._id).populate('couv_pic').exec(function (err, user) {

        if(user.couv_pic === null){
            res.json({success : true, message : 'pas d\'image a supprimé'});
            return;
        }
        Image.find().remove({ _id: user.couv_pic }).exec();
        if(user.couv_pic.name && user.couv_pic.name != "default"){
            fs.unlinkSync('./public/img/couvs/' + user.couv_pic.name);
        }
        User.findByIdAndUpdate(user._id, { $set: { couv_pic: null }}, { new: true }, function (err, user) {
            if (err) return next(err);
            res.json({success : true, message : 'L\'image a bien été supprimée'});
        });
    });    
}); 
//Allow to set an user id in the url 
router.param('user', param.getID);

module.exports = router;
